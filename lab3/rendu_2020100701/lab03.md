---
geometry:
- top=1in
- left=1in
- right=1in
- bottom=0.7in
---
# Laboratoire 3 de POO1-A

Groupe Mai_Palacios:

- Roosembert Palacios
- Hoang Anh Mai

## Schéma de classes

![Schéma de classes du laboratoire 3 « Lego »](./lab03.png)

\pagebreak

## Hypothèses

- On ne modélise pas une brique physique, mais plutôt un type de brique dont
  plusieurs briques physiques peuvent être fabriquées.
- Une boîte peut contenir une brique et un sachet qui contient la même brique.
- Une boîte peut contenir plusieurs types de briques.
- Un sachet contient forcément au moins une brique.
- Plusieurs briques peuvent avoir la même couleur.
- Tous les briques d'un sachet sont de la même gamme.
- Il est possible de vendre une boîte vide.
- Un manuel indique comment réaliser au moins un modèle.
- Un modèle peut ne pas être inclut dans aucun manuel.
- Les instructions pour construire le même modèle peuvent être retrouvés dans
  plusieurs manuels.
- Une étape de construction nécessite au moins une brique ou un sachet.
- Une étape de construction peut nécessiter la même brique plusieurs fois.

## Choix d'implémentation

- Deux manuels pour le même modèle dans des langues différentes sont considérés
  comme des manuels différents.
- La langue d'une boîte est obtenue à partir de la langue de son manuel.
- Un canal de vente étant mal défini, est représenté par une classe abstraite.
  Deux gabarit implémentant cette classe sont proposés, à raffiner avec le
  mandant.
- Les types collections ont été représentés par le nom du type contenu au pluriel.

## Organisation

### Répartition du travail

D'abord, nous avons travaillé séparément sur un schéma chacun.
Ensuite nous avons étudié le schéma de l'autre et nous nous sommes réunis pour
interpeller nos hypothèses et choix d'implémentation.
Ainsi nous avons pris un des schémas comme base et procédé à son raffinement.
Finalement nous avons rédigé le rapport ensemble et listé les points clés que
nous avons retenu lors de phase de conception.
Nous avons aussi remarqué quelques petits soucis mineurs que nous avons corrigé
dans le schéma.
