package engine;

import chess.*;
import chess.views.console.ConsoleView;
import chess.views.gui.GUIView;
import engine.game.Board;

public class Chess {

    private static void usage() {
        System.out.println("This program may be invoked with a single argument");
        System.out.println(" \"gui\" to display a graphical window");
        System.out.println(" \"tui\" to display a console-based interface");
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            usage();
            return;
        }

        ChessController controller = new Board();
        ChessView view;

        if ("gui".equals(args[0])) {
            view = new GUIView(controller);
        } else if ("tui".equals(args[0])) {
            view = new ConsoleView(controller);
        } else {
            usage();
            return;
        }
        controller.start(view);
    }
}
