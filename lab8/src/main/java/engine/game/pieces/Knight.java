package engine.game.pieces;

import chess.PieceType;
import chess.PlayerColor;
import engine.game.moves.basics.KnightMove;

public class Knight extends Piece {
    public Knight(PlayerColor color, int posX, int posY) {
        super(color, PieceType.KNIGHT, posX, posY);

        moves.add(new KnightMove());
    }
}
