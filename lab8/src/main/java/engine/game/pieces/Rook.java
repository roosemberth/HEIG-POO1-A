package engine.game.pieces;

import chess.PieceType;
import chess.PlayerColor;
import engine.game.moves.basics.LineMove;

public class Rook extends Piece {

    public Rook(PlayerColor color, int posX, int posY) {
        super(color, PieceType.ROOK, posX, posY);

        moves.add(new LineMove());

    }
}
