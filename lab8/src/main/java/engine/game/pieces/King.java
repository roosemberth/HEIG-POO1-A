package engine.game.pieces;

import chess.PieceType;
import chess.PlayerColor;
import engine.game.moves.basics.DiagonalMove;
import engine.game.moves.basics.LineMove;
import engine.game.moves.strategies.Castling;

public class King extends Piece {

    public King(PlayerColor color, int posX, int posY) {
        super(color, PieceType.KING, posX, posY);

        moves.add(new DiagonalMove());
        moves.add(new LineMove());
        moves.add(new Castling());
    }
}
