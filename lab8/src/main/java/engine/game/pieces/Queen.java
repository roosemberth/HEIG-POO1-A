package engine.game.pieces;

import chess.PieceType;
import chess.PlayerColor;
import engine.game.moves.basics.DiagonalMove;
import engine.game.moves.basics.LineMove;

public class Queen extends Piece {
    public Queen(PlayerColor color, int posX, int posY) {
        super(color, PieceType.QUEEN, posX, posY);

        moves.add(new LineMove());
        moves.add(new DiagonalMove());
    }
}
