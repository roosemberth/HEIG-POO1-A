package engine.game.pieces;

import chess.PlayerColor;
import chess.PieceType;
import engine.game.moves.Move;

import java.util.LinkedList;

public abstract class Piece {
    private final PlayerColor color;
    private final PieceType type;
    private boolean hasMoved;

    private int posX;
    private int posY;

    protected LinkedList<Move> moves;

    public Piece(PlayerColor color, PieceType type, int posX, int posY) {
        this.color = color;
        this.type = type;

        this.posX = posX;
        this.posY = posY;

        moves = new LinkedList<>();

        hasMoved = false;
    }

    public int getX() {
        return posX;
    }

    public int getY() {
        return posY;
    }

    public void setPosition(int x, int y) {
        posX = x;
        posY = y;
    }

    public PieceType getType() {
        return type;
    }

    public PlayerColor getColor() {
        return color;
    }

    public LinkedList<Move> getMoves() {
        return moves;
    }

    public void moved() {
        hasMoved = true;
    }

    public boolean hasMoved() {
        return hasMoved;
    }
}
