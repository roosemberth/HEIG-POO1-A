package engine.game.pieces;

import chess.PieceType;
import chess.PlayerColor;
import engine.game.moves.basics.DiagonalMove;

public class Bishop extends Piece {
    public Bishop(PlayerColor color, int posX, int posY) {
        super(color, PieceType.BISHOP, posX, posY);

        moves.add(new DiagonalMove());
    }
}
