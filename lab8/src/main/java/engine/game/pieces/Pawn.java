package engine.game.pieces;

import chess.PieceType;
import chess.PlayerColor;
import engine.game.moves.basics.PawnCaptureMove;
import engine.game.moves.basics.PawnMove;
import engine.game.moves.strategies.EnPassant;

public class Pawn extends Piece {

    private boolean enPassant;

    public Pawn(PlayerColor color, int posX, int posY) {
        super(color, PieceType.PAWN, posX, posY);

        moves.add(new PawnMove());
        moves.add(new PawnCaptureMove());
        moves.add(new EnPassant());

        enPassant = false;
    }

    public void setEnPassant() {
        enPassant = true;
    }

    public boolean isEnPassant() {
        return enPassant;
    }
}
