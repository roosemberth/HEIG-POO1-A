package engine.game;

import chess.ChessView;
import chess.PieceType;

public class PieceChoice implements ChessView.UserChoice {

    private final PieceType choice;

    public PieceChoice(PieceType type) {
        choice = type;
    }

    public String textValue() {
        switch (choice) {
            case QUEEN:
                return "Queen";
            case ROOK:
                return "Rook";
            case BISHOP:
                return "Bishop";
            case KNIGHT:
                return "Knight";
            default:
        }
        return null;
    }

    public PieceType getChoice() {
        return choice;
    }

    @Override
    public String toString() {
        return textValue();
    }
}
