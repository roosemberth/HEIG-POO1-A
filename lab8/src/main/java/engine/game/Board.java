package engine.game;

import chess.ChessController;
import chess.ChessView;
import chess.PieceType;
import chess.PlayerColor;
import engine.game.moves.Move;
import engine.game.moves.basics.PawnCaptureMove;
import engine.game.moves.basics.PawnMove;
import engine.game.moves.strategies.Castling;
import engine.game.moves.strategies.EnPassant;
import engine.game.pieces.*;

import java.util.Iterator;
import java.util.LinkedList;

import static chess.PieceType.*;
import static chess.PlayerColor.BLACK;
import static chess.PlayerColor.WHITE;

public class Board implements ChessController {

    private final int sizeBoard = 8;

    private Piece[][] grid;
    private LinkedList<Piece> pieces;

    private boolean whitePlayer;

    private ChessView boardView;


    public Board() {
        pieces = new LinkedList<>();
        grid = new Piece[sizeBoard][sizeBoard];

        // white plays first
        whitePlayer = true;

        startGame();
    }

    private void startGame() {
        pieces.clear();

        for (int y = 2; y < sizeBoard - 2; y++) {
            for (int x = 0; x < sizeBoard; x++) {
                grid[y][x] = null;
            }
        }

        createPieces(WHITE);
        createPieces(BLACK);

        for (int y = 0; y < sizeBoard; y++) {
            for (int x = 0; x < sizeBoard; x++) {
                if (grid[y][x] != null) {
                    pieces.add(grid[y][x]);
                }
            }
        }
    }

    @Override
    public void start(ChessView view) {
        boardView = view;
        for (int y = 0; y < sizeBoard; y++) {
            for (int x = 0; x < sizeBoard; x++) {
                if (grid[y][x] != null) {
                    boardView.putPiece(grid[y][x].getType(), grid[y][x].getColor(), x, y);
                }
            }
        }
        displayNextPlayer();
        boardView.startView();
    }

    @Override
    public boolean move(int fromX, int fromY, int toX, int toY) {
        PlayerColor color = whitePlayer ? WHITE : BLACK;

        Piece piece = grid[fromY][fromX];
        Piece dst = getPiece(toX, toY);

        // Empty square cannot be moved
        if (piece == null) {
            return false;
        }
        // Player cannot move opponent's pieces
        if (piece.getColor() != color) {
            return false;
        }

        LinkedList<Move> moves = piece.getMoves();
        Iterator<Move> it = moves.iterator();
        while (it.hasNext()) {
            Move move = it.next();
            if (move.canMove(this, fromX, fromY, toX, toY)) {
                if (move.getClass() == EnPassant.class) {
                    movePiece(piece, toX, toY);
                    int dY = color == WHITE ? 1 : -1;
                    Piece enPassant = getPiece(toX, toY - dY);
                    removeFromGrid(enPassant);

                    if (kingIsCheck(whitePlayer)) {
                        undoMove(piece, fromX, fromY);
                        putInGrid(enPassant, toX, toY - dY);
                        boardView.displayMessage("Move can't be done!");
                        return false;
                    }
                } else if (move.getClass() == Castling.class) {
                    movePiece(piece, toX, toY);
                    int dX = toX - fromX;

                    int leftEnd = 0;
                    int rightEnd = 7;
                    int posX = dX > 0 ? rightEnd : leftEnd;
                    int newPosX = dX > 0 ? toX - 1 : toX + 1;
                    Rook rook = (Rook) getPiece(posX, fromY);
                    movePiece(rook, newPosX, toY);

                    if (kingIsCheck(whitePlayer)) {
                        undoMove(rook, posX, fromY);
                        undoMove(piece, fromX, fromY);
                        boardView.displayMessage("Move can't be done!");
                        return false;
                    }
                } else {
                    if (dst != null) {
                        removeFromGrid(dst);
                    }

                    // Promotion move
                    if (move.getClass() == PawnMove.class || move.getClass() == PawnCaptureMove.class) {
                        int upRow = 7;
                        int downRow = 0;

                        if (toY == upRow || toY == downRow) {
                            removeFromGrid(piece);

                            PieceChoice[] possibilities = {new PieceChoice(QUEEN), new PieceChoice(ROOK),
                                    new PieceChoice(KNIGHT), new PieceChoice(BISHOP)};
                            PieceChoice promoChoice = boardView.askUser("Promotion !",
                                    "Promote to ?", possibilities);
                            Piece newPiece = switch (promoChoice.getChoice()) {
                                case QUEEN -> new Queen(color, toX, toY);
                                case ROOK -> new Rook(color, toX, toY);
                                case BISHOP -> new Bishop(color, toX, toY);
                                case KNIGHT -> new Knight(color, toX, toY);
                                default -> throw new RuntimeException("Invalid piece !");
                            };

                            putInGrid(newPiece, toX, toY);

                            if (kingIsCheck(whitePlayer)) {
                                removeFromGrid(newPiece);
                                putInGrid(piece, fromX, fromY);
                                boardView.displayMessage("Move can't be done!");
                                return false;
                            }

                        } else {
                            movePiece(piece, toX, toY);
                            if (piece.getType() == PieceType.PAWN && Math.abs(toY - fromY) == 2) {
                                ((Pawn) piece).setEnPassant();
                            }
                        }
                    } else {
                        movePiece(piece, toX, toY);
                    }
                }

                if (kingIsCheck(whitePlayer)) {
                    undoMove(piece, fromX, fromY);
                    if (dst != null) {
                        putInGrid(dst, toX, toY);
                    }
                    boardView.displayMessage("Move can't be done!");
                    return false;
                }

                piece.moved();

                changePlayer();

                if (kingIsCheck(whitePlayer)) {
                    boardView.displayMessage("Check !");
                }
                return true;
            }
        }
        return false;
    }

    private void displayNextPlayer() {
        if (whitePlayer)
          boardView.displayMessage("It's white's turn!");
        else
          boardView.displayMessage("It's black's turn!");
    }

    private void changePlayer() {
        whitePlayer = !whitePlayer;
        displayNextPlayer();
    }

    private void movePiece(Piece piece, int toX, int toY) {
        grid[piece.getY()][piece.getX()] = null;
        boardView.removePiece(piece.getX(), piece.getY());
        grid[toY][toX] = piece;
        piece.setPosition(toX, toY);
        boardView.putPiece(piece.getType(), piece.getColor(), toX, toY);
    }

    private void undoMove(Piece piece, int x, int y) {
        grid[piece.getY()][piece.getX()] = null;
        boardView.removePiece(piece.getX(), piece.getY());
        piece.setPosition(x, y);
        grid[y][x] = piece;
        boardView.putPiece(piece.getType(), piece.getColor(), x, y);
    }

    private void removeFromGrid(Piece piece) {
        grid[piece.getY()][piece.getX()] = null;
        pieces.remove(piece);
        boardView.removePiece(piece.getX(), piece.getY());
    }

    private void putInGrid(Piece piece, int x, int y) {
        grid[y][x] = piece;
        piece.setPosition(x, y);
        pieces.add(piece);
        boardView.putPiece(piece.getType(), piece.getColor(), piece.getX(), piece.getY());
    }

    private void createPieces(PlayerColor color) throws RuntimeException {
        int y;
        switch (color) {
            case WHITE -> {
                y = 1;
                for (int x = 0; x < sizeBoard; x++) {
                    grid[y][x] = new Pawn(color, x, y);
                }
                y = 0;
                grid[y][0] = new Rook(color, 0, y);
                grid[y][1] = new Knight(color, 1, y);
                grid[y][2] = new Bishop(color, 2, y);
                grid[y][3] = new Queen(color, 3, y);
                grid[y][4] = new King(color, 4, y);
                grid[y][5] = new Bishop(color, 5, y);
                grid[y][6] = new Knight(color, 6, y);
                grid[y][7] = new Rook(color, 7, y);
            }
            case BLACK -> {
                y = 6;
                for (int x = 0; x < sizeBoard; x++) {
                    grid[y][x] = new Pawn(color, x, y);
                }
                y = 7;
                grid[y][0] = new Rook(color, 0, y);
                grid[y][1] = new Knight(color, 1, y);
                grid[y][2] = new Bishop(color, 2, y);
                grid[y][3] = new Queen(color, 3, y);
                grid[y][4] = new King(color, 4, y);
                grid[y][5] = new Bishop(color, 5, y);
                grid[y][6] = new Knight(color, 6, y);
                grid[y][7] = new Rook(color, 7, y);
            }
            default -> throw new RuntimeException("Invalid player's color");
        }
    }

    @Override
    public void newGame() {
        startGame();
        whitePlayer = true;
        start(boardView);
    }

    public Piece getPiece(int x, int y) {
        if (x < 0 || y < 0 || x >= 8 || y >= 8) {
            return null;
        }
        return grid[y][x];
    }

    public King getKing(boolean player) {
        PlayerColor color = player ? WHITE : BLACK;

        Iterator<Piece> it = pieces.iterator();
        while (it.hasNext()) {
            Piece piece = it.next();
            if (piece.getType() == PieceType.KING && piece.getColor() == color) {
                return (King) piece;
            }
        }

        return null;
    }

    public boolean kingIsCheck(boolean white) {

        return diagonalAttack(white) || knightAttack(white) || lineAttack(white);
    }

    public boolean diagonalAttack(boolean white) {
        King king = getKing(white);

        // PAWN attack and KING attack
        int dirY = white ? 1 : -1;
        Piece p = getPiece(king.getX() + 1, king.getY() + dirY);
        if (p != null) {
            if (p.getType() == PAWN || p.getType() == KING) {
                if (grid[king.getY() + dirY][king.getX() + 1].getColor() != king.getColor()) {
                    return true;
                }
            }
        }

        p = getPiece(king.getX() - 1, king.getY() + dirY);
        if (p != null) {
            if (p.getType() == PAWN || p.getType() == KING) {
                if (p.getColor() != king.getColor()) {
                    return true;
                }
            }
        }

        p = getPiece(king.getX() + 1, king.getY() - dirY);
        if (p != null) {
            if (p.getType() == KING) {
                if (p.getColor() != king.getColor()) {
                    return true;
                }
            }
        }

        p = getPiece(king.getX() - 1, king.getY() - dirY);
        if (p != null) {
            if (p.getType() == KING) {
                if (p.getColor() != king.getColor()) {
                    return true;
                }
            }
        }

        int i = king.getY() + 1;
        int j = king.getX() + 1;
        p = null;
        while (i < sizeBoard && j < sizeBoard && p == null) {
            p = getPiece(j, i);
            if (p != null) {
                if (p.getType() == BISHOP || p.getType() == QUEEN) {
                    if (king.getColor() != p.getColor()) {
                        return true;
                    }
                }
            }
            i++;
            j++;
        }

        i = king.getY() - 1;
        j = king.getX() + 1;
        p = null;
        while (i >= 0 && j < sizeBoard && p == null) {
            p = getPiece(j, i);
            if (p != null) {
                if (p.getType() == BISHOP || p.getType() == QUEEN) {
                    if (king.getColor() != p.getColor()) {
                        return true;
                    }
                }
            }
            i--;
            j++;
        }

        i = king.getY() - 1;
        j = king.getX() - 1;
        p = null;
        while (i >= 0 && j >= 0 && p == null) {
            p = getPiece(j, i);
            if (p != null) {
                if (p.getType() == BISHOP || p.getType() == QUEEN) {
                    if (king.getColor() != p.getColor()) {
                        return true;
                    }
                }
            }
            i--;
            j--;
        }

        i = king.getY() + 1;
        j = king.getX() - 1;
        p = null;
        while (i < sizeBoard && j >= 0 && p == null) {
            p = getPiece(j, i);
            if (p != null) {
                if (p.getType() == BISHOP || p.getType() == QUEEN) {
                    if (king.getColor() != p.getColor()) {
                        return true;
                    }
                }
            }
            i++;
            j--;
        }

        return false;
    }

    public boolean lineAttack(boolean white) {
        King king = getKing(white);

        // Check for KING attack
        Piece p = getPiece(king.getX(), king.getY() + 1);
        if (p != null) {
            if (p.getType() == KING) {
                return true;
            }
        }

        p = getPiece(king.getX(), king.getY() - 1);
        if (p != null) {
            if (p.getType() == KING) {
                return true;
            }
        }

        p = getPiece(king.getX() + 1, king.getY());
        if (p != null) {
            if (p.getType() == KING) {
                return true;
            }
        }

        p = getPiece(king.getX() - 1, king.getY());
        if (p != null) {
            if (p.getType() == KING) {
                return true;
            }
        }

        int i = king.getX() + 1;
        p = null;
        while (i < sizeBoard && p == null) {
            p = getPiece(i, king.getY());
            if (p != null) {
                if (p.getType() == ROOK || p.getType() == QUEEN) {
                    if (king.getColor() != p.getColor()) {
                        return true;
                    }
                }
            }
            i++;
        }

        i = king.getX() - 1;
        p = null;
        while (i >= 0 && p == null) {
            p = getPiece(i, king.getY());
            if (p != null) {
                if (p.getType() == ROOK || p.getType() == QUEEN) {
                    if (king.getColor() != p.getColor()) {
                        return true;
                    }
                }
            }
            i--;
        }

        i = king.getY() + 1;
        p = null;
        while (i < sizeBoard && p == null) {
            p = getPiece(king.getX(), i);
            if (p != null) {
                if (p.getType() == ROOK || p.getType() == QUEEN) {
                    if (king.getColor() != p.getColor()) {
                        return true;
                    }
                }
            }
            i++;
        }

        i = king.getY() - 1;
        p = null;
        while (i >= 0 && p == null) {
            p = getPiece(king.getX(), i);
            if (p != null) {
                if (p.getType() == ROOK || p.getType() == QUEEN) {
                    if (king.getColor() != p.getColor()) {
                        return true;
                    }
                }
            }
            i--;
        }

        return false;
    }

    public boolean knightAttack(boolean white) {
        King king = getKing(white);

        Piece knight = getPiece(king.getX() + 2, king.getY() + 1);
        if (knight != null) {
            if (knight.getType() == KNIGHT) {
                if (king.getColor() != knight.getColor()) {
                    return true;
                }
            }
        }

        knight = getPiece(king.getX() + 2, king.getY() - 1);
        if (knight != null) {
            if (knight.getType() == KNIGHT) {
                if (king.getColor() != knight.getColor()) {
                    return true;
                }
            }
        }

        knight = getPiece(king.getX() + 1, king.getY() + 2);
        if (knight != null) {
            if (knight.getType() == KNIGHT) {
                if (king.getColor() != knight.getColor()) {
                    return true;
                }
            }
        }

        knight = getPiece(king.getX() - 1, king.getY() + 2);
        if (knight != null) {
            if (knight.getType() == KNIGHT) {
                if (king.getColor() != knight.getColor()) {
                    return true;
                }
            }
        }

        knight = getPiece(king.getX() - 2, king.getY() + 1);
        if (knight != null) {
            if (knight.getType() == KNIGHT) {
                if (king.getColor() != knight.getColor()) {
                    return true;
                }
            }
        }

        knight = getPiece(king.getX() - 2, king.getY() - 1);
        if (knight != null) {
            if (knight.getType() == KNIGHT) {
                if (king.getColor() != knight.getColor()) {
                    return true;
                }
            }
        }

        knight = getPiece(king.getX() - 1, king.getY() - 2);
        if (knight != null) {
            if (knight.getType() == KNIGHT) {
                if (king.getColor() != knight.getColor()) {
                    return true;
                }
            }
        }

        knight = getPiece(king.getX() + 1, king.getY() - 2);
        if (knight != null) {
            if (knight.getType() == KNIGHT) {
                if (king.getColor() != knight.getColor()) {
                    return true;
                }
            }
        }

        return false;
    }
}
