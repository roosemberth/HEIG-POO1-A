package engine.game.moves;

import engine.game.Board;
import engine.game.pieces.Piece;

public abstract class Move {

    public boolean inBoard(int toX, int toY) {
        int sizeBoard = 8;
        return toX < sizeBoard && toX >= 0 && toY < sizeBoard && toY >= 0;
    }

    public boolean canMove(Board board, int fromX, int fromY, int toX, int toY) {
        int dX = toX - fromX;
        int dY = toY - fromY;

        if (dX == 0 && dY == 0) {
            return false;
        }
        return inBoard(toX, toY);
    }

    public boolean canCapture(Piece src, Piece dst) {
        if (src == null || dst == null) {
            return false;
        }
        return src.getColor() != dst.getColor();
    }
}
