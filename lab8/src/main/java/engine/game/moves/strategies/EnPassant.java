package engine.game.moves.strategies;

import chess.PieceType;
import engine.game.Board;
import engine.game.moves.Move;
import engine.game.pieces.Pawn;
import engine.game.pieces.Piece;

public class EnPassant extends Move {

    @Override
    public boolean canMove(Board board, int fromX, int fromY, int toX, int toY) {

        if (!super.canMove(board, fromX, fromY, toX, toY)) {
            return false;
        }

        int dX = toX - fromX;
        int dY = toY - fromY;

        if (!(Math.abs(dX) == 1 && Math.abs(dY) == 1)) {
            return false;
        }

        Piece src = board.getPiece(fromX, fromY);

        Piece enPassant = board.getPiece(toX, toY - dY);

        if (enPassant.getType() != PieceType.PAWN) {
            return false;
        } else if (!((Pawn) enPassant).isEnPassant()) {
            return false;
        } else if (!canCapture(src, enPassant)) {
            return false;
        }
        return true;
    }
}
