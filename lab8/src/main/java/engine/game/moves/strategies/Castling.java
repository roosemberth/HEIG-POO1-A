package engine.game.moves.strategies;

import engine.game.Board;
import engine.game.moves.Move;
import engine.game.pieces.King;
import engine.game.pieces.Piece;
import engine.game.pieces.Rook;

import static chess.PieceType.KING;

public class Castling extends Move {

    @Override
    public boolean canMove(Board board, int fromX, int fromY, int toX, int toY) {

        if (!super.canMove(board, fromX, fromY, toX, toY)) {
            return false;
        }
        int dX = toX - fromX;
        int dY = toY - fromY;

        int downRow = 0;
        int upRow = 7;
        if ((toY != downRow && toY != upRow) || dY != 0 || Math.abs(dX) != 2) {
            return false;
        }


        Piece piece = board.getPiece(fromX, fromY);

        if (piece.hasMoved()) {
            return false;
        }
        if (piece.getType() == KING) {
            int leftEnd = 0;
            int rightEnd = 7;
            int posX = dX > 0 ? rightEnd : leftEnd;
            Piece rook = board.getPiece(posX, fromY);
            if (rook.getClass() != Rook.class) {
                return false;
            } else if (((Rook) rook).hasMoved()) {
                return false;
            }

            if (dX > 0) {
                for (int i = fromX + 1; i < posX; i++) {
                    if (board.getPiece(i, fromY) != null) {
                        return false;
                    }
                }
            } else {
                for (int i = fromX - 1; i > posX; i--) {
                    if (board.getPiece(i, fromY) != null) {
                        return false;
                    }
                }
            }
        }


        return true;
    }
}
