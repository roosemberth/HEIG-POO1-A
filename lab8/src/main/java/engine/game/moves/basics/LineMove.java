package engine.game.moves.basics;

import chess.PieceType;
import engine.game.Board;
import engine.game.moves.Move;
import engine.game.pieces.Piece;

public class LineMove extends Move {

    @Override
    public boolean canMove(Board board, int fromX, int fromY, int toX, int toY) {

        if (!super.canMove(board, fromX, fromY, toX, toY)) {
            return false;
        }

        int dX = toX - fromX;
        int dY = toY - fromY;

        if (dX != 0 && dY != 0) {
            return false;
        }

        Piece piece = board.getPiece(fromX, fromY);
        Piece dst = board.getPiece(toX, toY);
        if (piece.getType() == PieceType.KING) {
            if (Math.abs(dX) + Math.abs(dY) != 1) {
                return false;
            }
        } else {
            if (dX == 0) {
                int dirY = dY / Math.abs(dY);

                for (int i = fromY + 1; i < toY; i += dirY) {
                    if (board.getPiece(fromX, i) != null) {
                        System.out.println("testing");
                        return false;
                    }
                }
            } else {
                int dirX = dX / Math.abs(dX);

                for (int i = fromX + 1; i < toX; i += dirX) {
                    if (board.getPiece(i, fromY) != null) {
                        return false;
                    }
                }
            }
        }

        if (dst != null) {
            if (!canCapture(piece, dst)) {
                return false;
            }
        }

        return true;
    }
}
