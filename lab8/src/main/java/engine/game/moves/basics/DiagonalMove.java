package engine.game.moves.basics;

import chess.PieceType;
import engine.game.Board;
import engine.game.moves.Move;
import engine.game.pieces.Piece;

public class DiagonalMove extends Move {

    @Override
    public boolean canMove(Board board, int fromX, int fromY, int toX, int toY) {

        if (!super.canMove(board, fromX, fromY, toX, toY)) {
            return false;
        }

        int dX = toX - fromX;
        int dY = toY - fromY;

        if (Math.abs(dX) != Math.abs(dY)) {
            return false;
        } else if (Math.abs(dX) == Math.abs(dY)) {
            if (Math.abs(dX) == 0) {
                return false;
            }
        }

        Piece piece = board.getPiece(fromX, fromY);
        Piece dst = board.getPiece(toX, toY);

        if (piece.getType() == PieceType.KING) {
            if (Math.abs(dX) + Math.abs(dY) != 2) {
                return false;
            }
        } else {
            int dirX = dX / Math.abs(dX);
            int dirY = dY / Math.abs(dY);

            for (int i = fromX + 1, j = fromY + 1; i < toX && j < dY; i += dirX, j += dirY) {
                if (board.getPiece(i, j) != null) {
                    return false;
                }
            }
        }

        if (dst != null) {
            if (!canCapture(piece, dst)) {
                return false;
            }
        }

        return true;
    }
}
