package engine.game.moves.basics;

import engine.game.Board;
import engine.game.moves.Move;
import engine.game.pieces.Piece;

public class KnightMove extends Move {

    @Override
    public boolean canMove(Board board, int fromX, int fromY, int toX, int toY) {

        if (!super.canMove(board, fromX, fromY, toX, toY)) {
            return false;
        }

        int dX = toX - fromX;
        int dY = toY - fromY;

        if (Math.abs(dX) + Math.abs(dY) != 3) {
            return false;
        }

        Piece knight = board.getPiece(fromX, fromY);
        Piece dst = board.getPiece(toX, toY);

        switch (Math.abs(dX)) {
            case 1:
                if (Math.abs(dY) != 2) {
                    return false;
                }
                break;
            case 2:
                if (Math.abs(dY) != 1) {
                    return false;
                }
                break;
            default:
        }

        if (dst != null) {
            if (!canCapture(knight, dst)) {
                return false;
            }
        }

        return true;
    }
}
