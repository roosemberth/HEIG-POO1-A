package engine.game.moves.basics;

import engine.game.Board;
import engine.game.moves.Move;
import engine.game.pieces.Piece;

public class PawnCaptureMove extends Move {

    @Override
    public boolean canMove(Board board, int fromX, int fromY, int toX, int toY) {

        if (!super.canMove(board, fromX, fromY, toX, toY)) {
            return false;
        }

        int dX = toX - fromX;
        int dY = toY - fromY;

        if (Math.abs(dX) != Math.abs(dY) || !(Math.abs(dX) == Math.abs(dY) && Math.abs(dX) == 1)) {
            return false;
        }

        Piece pawn = board.getPiece(fromX, fromY);
        Piece toCapture = board.getPiece(toX, toY);
        if (toCapture == null) {
            return false;
        } else if (!canCapture(pawn, toCapture)) {
            return false;
        }

        return true;
    }
}
