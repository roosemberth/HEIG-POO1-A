package engine.game.moves.basics;

import engine.game.Board;
import engine.game.moves.Move;
import engine.game.pieces.Piece;

public class PawnMove extends Move {

    @Override
    public boolean canMove(Board board, int fromX, int fromY, int toX, int toY) {

        if (!super.canMove(board, fromX, fromY, toX, toY)) {
            return false;
        }

        int maxMoves = 2;

        int dX = toX - fromX;
        int dY = toY - fromY;

        if (Math.abs(dX) != 0 || Math.abs(dY) == 0) {
            return false;
        }

        int dirY = dY / Math.abs(dY);

        Piece piece = board.getPiece(fromX, fromY + dirY);
        if (piece != null) {
            return false;
        }


        Piece pawn = board.getPiece(fromX, fromY);
        if (pawn.hasMoved()) {
            if (Math.abs(dY) >= maxMoves) {
                return false;
            }
        } else {
            if (Math.abs(dY) > maxMoves) {
                return false;
            }
        }

        return true;
    }
}
