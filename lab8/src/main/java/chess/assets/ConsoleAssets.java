package chess.assets;

import chess.PieceType;
import chess.PlayerColor;
import chess.views.console.ConsoleView;

public class ConsoleAssets {

    public static void loadAssets(ConsoleView v) {
        registerPieceResource(v, PieceType.ROOK, PlayerColor.BLACK, "R");
        registerPieceResource(v, PieceType.ROOK, PlayerColor.WHITE, "R");

        registerPieceResource(v, PieceType.PAWN, PlayerColor.BLACK, "P");
        registerPieceResource(v, PieceType.PAWN, PlayerColor.WHITE, "P");

        registerPieceResource(v, PieceType.KNIGHT, PlayerColor.BLACK, "N");
        registerPieceResource(v, PieceType.KNIGHT, PlayerColor.WHITE, "N");

        registerPieceResource(v, PieceType.BISHOP, PlayerColor.BLACK, "B");
        registerPieceResource(v, PieceType.BISHOP, PlayerColor.WHITE, "B");

        registerPieceResource(v, PieceType.QUEEN, PlayerColor.BLACK, "Q");
        registerPieceResource(v, PieceType.QUEEN, PlayerColor.WHITE, "Q");

        registerPieceResource(v, PieceType.KING, PlayerColor.BLACK, "K");
        registerPieceResource(v, PieceType.KING, PlayerColor.WHITE, "K");
    }

    private static void registerPieceResource(ConsoleView v, PieceType type, PlayerColor color, String letter) {
        v.registerResource(type, color, ConsoleView.createResource(letter, color));
    }
}
