package chess.assets;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import chess.PieceType;
import chess.PlayerColor;
import chess.views.gui.GUIView;

public class GuiAssets {
    public static void loadAssets(GUIView v) {
        try {
            registerPieceResource(v, PieceType.ROOK, PlayerColor.BLACK, "rook_black.png");
            registerPieceResource(v, PieceType.ROOK, PlayerColor.WHITE, "rook_white.png");

            registerPieceResource(v, PieceType.PAWN, PlayerColor.BLACK, "pawn_black.png");
            registerPieceResource(v, PieceType.PAWN, PlayerColor.WHITE, "pawn_white.png");

            registerPieceResource(v, PieceType.KNIGHT, PlayerColor.BLACK, "knight_black.png");
            registerPieceResource(v, PieceType.KNIGHT, PlayerColor.WHITE, "knight_white.png");

            registerPieceResource(v, PieceType.BISHOP, PlayerColor.BLACK, "bishop_black.png");
            registerPieceResource(v, PieceType.BISHOP, PlayerColor.WHITE, "bishop_white.png");

            registerPieceResource(v, PieceType.QUEEN, PlayerColor.BLACK, "queen_black.png");
            registerPieceResource(v, PieceType.QUEEN, PlayerColor.WHITE, "queen_white.png");

            registerPieceResource(v, PieceType.KING, PlayerColor.BLACK, "king_black.png");
            registerPieceResource(v, PieceType.KING, PlayerColor.WHITE, "king_white.png");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static void registerPieceResource(GUIView v, PieceType type, PlayerColor color, String imageName)
            throws IOException {
        v.registerResource(type, color, GUIView.createResource(assetsImage(imageName)));
    }

    private static BufferedImage assetsImage(String imageName) throws IOException {
        return ImageIO.read(GuiAssets.class.getClassLoader().getResource("images/" + imageName));
    }
}
