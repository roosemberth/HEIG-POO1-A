# Exercise ObjectList

To run tests:
```
$ mvn clean compile test
```

To run showcase application:
```
$ mvn clean compile exec:java -Dexec.mainClass=main.ObjectListShow
```
