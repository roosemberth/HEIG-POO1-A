package main;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class ObjectListTest {
  @Test
  void testInsertGetRemove() {
    ObjectList l = new ObjectList();
    Object so = new Object();
    l.insert(new Object());
    l.insert(so);
    l.insert(new Object());
    assertEquals(so, l.get(1));
    assertEquals(3, l.size());
    l.remove(so);
    assertEquals(2, l.size());
    assertThrows(RuntimeException.class, () -> {l.remove(so);});
  }

  @Test
  void testMultiInsertAppend() {
    ObjectList l = new ObjectList();
    Object so = new Object();
    for (int i = 0; i < 10; ++i) {
      l.append(so);
    }
    for (int i = 0; i < 10; ++i) {
      l.insert(so);
    }
    assertEquals(20, l.size());
    for (int i = 0; i < 20; ++i) {
      assertDoesNotThrow(() -> {l.remove(so);});
    }
    assertThrows(IndexOutOfBoundsException.class, () -> {l.get(0);});
  }

  @Test
  void testExaminatorIteratesAllMembers() {
    ObjectList l = new ObjectList();
    Object so = new Object();
    Examinator e = l.examinator();
    l.insert(so);
    assertTrue(l.examinator().hasNext());

    for (int i = 0; i < 9; ++i) {
      l.insert(so);
    }

    e = l.examinator();
    for (int i = 0; i < 10; ++i) {
      assertTrue(e.hasNext());
      e.next();
    }
  }
}
