// FIXME: Unchecked exceptions are evil.
package main;

class Element
{
  Object data;
  Element next;

  Element(Object o) {
    if (o == null) {
      throw new IllegalArgumentException("A list's element data cannot be null.");
    }
    data = o;
  }
}

class ObjectList
{
  private Element head;

  public boolean isEmpty() {
    return head == null;
  }

  public int size() {
    Examinator e = examinator();
    int size = 0;
    while (e.hasNext()) {
      size = size + 1;
      e.next();
    }
    return size;
  }

  public void insert(Object o) {
    Element newHead = new Element(o);
    newHead.next = head;
    head = newHead;
  }

  public void append(Object o) {
    if (head == null) {
      // List was empty
      head = new Element(o);
      return;
    }
    Element e = head;
    while (e.next != null)
      e = e.next;
    e.next = new Element(o);
  }

  public void remove(Object o) {
    // A RuntimeException reference prevents unchecked exception warning.
    RuntimeException objectDoesNotExist =
      new RuntimeException("The object to be removed is not in the list");

    if (head == null) {
      // List is empty.
      throw objectDoesNotExist;
    }

    Element prev = null;
    Element cur = head;

    while (cur.next != null && cur.data != o) {
      prev = cur;
      cur = cur.next;
    }

    if (cur.data != o) {
      // Object not found.
      throw objectDoesNotExist;
    }

    if (prev == null) {
      head = cur.next;
    } else {
      prev.next = cur.next;
    }
  }

  public Object get(int index) {
    Element e = head;

    while (e != null && index > 0) {
      index = index - 1;
      e = e.next;
    }

    if (index > 0 || e == null) {
      // The specified element was not found.
      throw new IndexOutOfBoundsException();
    }

    return e.data;
  }

  public String toString() {
    Examinator e = examinator();
    StringBuilder b = new StringBuilder();
    b.append("[");

    while (e.hasNext()) {
      b.append(e.next());
      if (!e.hasNext()) {
        break;
      }
      b.append(", ");
    }

    b.append("]");
    return b.toString();
  }

  public Examinator examinator() {
    return new Examinator(head);
  }
}

class Examinator
{
  private Element current;

  Examinator(Element start) {
    this.current = start;
  }

  boolean hasNext() {
    return current != null;
  }

  Object next() {
    Object ret = current.data;
    current = current.next;
    return ret;
  }
}
