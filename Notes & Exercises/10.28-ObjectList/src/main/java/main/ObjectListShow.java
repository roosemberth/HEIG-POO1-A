package main;

public class ObjectListShow {
  static class Foo {
    @Override
    public String toString() {
      return "Un Foo";
    }
  }
  static class Bar {
    @Override
    public String toString() {
      return "Un Bar";
    }
  }

  public static void main(String... args) {
    System.out.println("Création d'une liste !");
    ObjectList l = new ObjectList();
    System.out.println("Ajout d'un Foo à la fin.");
    l.append(new Foo());
    System.out.println("Ajout d'un Bar à la fin.");
    l.append(new Bar());
    System.out.println("Ajout d'un Bar au début.");
    l.insert(new Bar());
    System.out.println("La liste contient: " + l);
  }
}
