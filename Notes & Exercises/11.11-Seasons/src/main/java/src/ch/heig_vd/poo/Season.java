package ch.heig_vd.poo;

import java.util.List;
import java.util.Iterator;
import java.util.LinkedList;

public class Season {
  private static Season[] seasons = new Season[0];

  public static final Season Spring = new Season("Spring");
  public static final Season Summer = new Season("Summer");
  public static final Season Fall = new Season("Fall");
  public static final Season Winter = new Season("Winter");

  public final String name;
  public final int number;

  public Season(String name) {
    this.name = name;
    this.number = seasons.length;
    Season[] old = seasons;

    this.seasons = new Season[old.length+1];
    for (int i = 0; i < old.length; ++i)
      this.seasons[i] = old[i];
    this.seasons[old.length] = this;
  }

  public static Season[] values() {
    Season[] ret = new Season[seasons.length];
    for (int i = 0; i < seasons.length; ++i)
      ret[i] = seasons[i];
    return ret;
  }

  public Season next() {
    if (number == seasons.length - 1)
      return seasons[0];
    return seasons[number+1];
  }

  public Season previous() {
    if (number == 0)
      return seasons[seasons.length - 1];
    return seasons[number-1];
  }

  public static Season valueOf(String name) {
    for (Season s : seasons) {
      if (s.name == name)
        return s;
    }
    throw new RuntimeException("ValueOf unexisting season");
  }

  public static List<Season> range(Season from, Season to) {
    List r = new LinkedList<Season>();

    for (Season s = from; s != to; s = s.next()) {
      System.out.println(s);
      r.add(s);
    }
    r.add(to);
    return r;
  }

  @Override
  public String toString() {
    return name;
  }
}
