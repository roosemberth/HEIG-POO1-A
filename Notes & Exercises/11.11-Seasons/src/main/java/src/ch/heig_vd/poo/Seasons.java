package ch.heig_vd.poo;

public class Seasons {
  public static void main(String ... args) {
    for (Season s : Season.values())
      System.out.println(s);
    for (Season s : Season.range(Season.Spring, Season.Fall)) 
      System.out.printf("%10s <- %10s -> %10s\n", s.previous(), s, s.next());
    Season winter = Season.valueOf("Winter");
    if (winter == Season.Winter)
      System.out.println("Winter is coming");
  }
}
