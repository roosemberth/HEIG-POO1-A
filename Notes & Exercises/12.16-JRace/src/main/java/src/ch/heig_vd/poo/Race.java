package ch.heig_vd.poo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

class RaceEvent {
  private int winner = -1;
  private long time;

  public RaceEvent(long time) {
    this.time = time;
  }

  public RaceEvent(long time, int winner) {
    this(time);
    this.winner = winner;
  }

  public int getWinner() {
    return winner;
  }

  public long getTime() {
    return time;
  }
}

interface RaceListener {
  void action(RaceEvent e);
}

class JRace extends JPanel implements ActionListener, MouseListener {
  private static class RectLocation {
    final public int minX, minY, maxX, maxY;

    public RectLocation(int minX, int minY, int maxX, int maxY) {
      this.minX = minX;
      this.minY = minY;
      this.maxX = maxX;
      this.maxY = maxY;
    }
  }

  private static class Runner {
    static Random random = new Random();
    static int count = 0;
    private boolean isPaused = false;

    int number = count++;
    double position = 0;

    void move() {
      if (position < 100 && !isPaused)
        position += random.nextDouble();
    }

    public void pause() {
      isPaused = true;
    }

    public void unPause() {
      isPaused = false;
    }

    public boolean isPaused() {
      return this.isPaused;
    }
  }

  private static Color[] colors = { Color.RED, Color.GREEN, Color.BLUE, Color.BLACK, Color.MAGENTA };
  private Runner runners[] = new Runner[10];
  private RectLocation runnerLocations[] = new RectLocation[10];
  private Timer timer;
  private RaceListener raceListener;
  private long startTime;
  private Runner pausedRunner = null;

  JRace() {
    setBackground(Color.WHITE);
    this.addMouseListener(this);
  }

  public void addRaceListener(RaceListener l) {
    // Simple implementation: only one listener at a time...
    raceListener = l;
  }

  public Dimension getPreferredSize() {
    return new Dimension(640, 480);
  }

  public void actionPerformed(ActionEvent e) {
    Runner winner = null;

    for (int i = 0; i < runners.length; i++) {
      runners[i].move();
      if (runners[i].position >= 100 && (winner == null || runners[i].position > winner.position))
        winner = runners[i];
    }
    if (raceListener != null)
      if (winner != null) {
        raceListener.action(new RaceEvent(System.currentTimeMillis() - startTime, winner.number));
        timer.stop();
      } else
        raceListener.action(new RaceEvent(System.currentTimeMillis() - startTime));
    repaint();
  }

  public void run() {
    Runner.count = 0;

    for (int i = 0; i < runners.length; i++)
      runners[i] = new Runner();

    timer = new Timer(50, this);
    timer.start();
    startTime = System.currentTimeMillis();
  }

  public void paintComponent(Graphics g) {
    int width = getWidth() - 40; // borders
    double ySize = getHeight() / (runners.length * 2 + 1.0);

    super.paintComponent(g);

    for (int i = 0; i < runners.length; i++)
      if (runners[i] != null) {
        if (runners[i].isPaused()) {
          g.setColor(Color.GRAY);
        } else {
          g.setColor(colors[i % colors.length]);
        }
        int rx, ry, rw, rh;
        rx = 20;
        ry = (int) (ySize * (1 + 2 * i));
        rw = Math.min((int) runners[i].position, 100) * width / 100;
        rh = (int) ySize;
        g.fillRect(rx, ry, rw, rh);
        runnerLocations[i] = new RectLocation(rx, ry, rw + rx, rh + ry);
      }
    g.setColor(Color.BLACK);
    g.drawLine(20, 0, 20, getHeight());
    g.drawLine(width + 20, 0, width + 20, getHeight());
  }

  @Override
  public void mouseClicked(MouseEvent e) {
  }

  @Override
  public void mousePressed(MouseEvent e) {
    int x = e.getX(), y = e.getY();
    pausedRunner = null;
    for (int i = 0; i < runnerLocations.length; i++) {
      RectLocation r = runnerLocations[i];
      if (r != null) {
        if (r.minX < x && r.maxX > x && r.minY < y && r.maxY > y) {
          pausedRunner = runners[i];
          break;
        }
      }
    }
    if (pausedRunner != null)
      pausedRunner.pause();
  }

  @Override
  public void mouseReleased(MouseEvent e) {
    if (pausedRunner != null)
      pausedRunner.unPause();
  }

  @Override
  public void mouseEntered(MouseEvent e) {
  }

  @Override
  public void mouseExited(MouseEvent e) {
  }
}

public class Race {
  public static void main(String[] args) {
    // try {
    // UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    // } catch (Exception e) { }

    final JRace race = new JRace();
    JFrame frame = new JFrame("Race");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    frame.getContentPane().add(race, BorderLayout.CENTER);

    JPanel south = new JPanel();
    south.setLayout(new GridLayout(0, 3));
    frame.getContentPane().add(south, BorderLayout.SOUTH);

    final JLabel label = new JLabel("Race not started", JLabel.CENTER);
    final JLabel clock = new JLabel("0.0", JLabel.CENTER);
    JPanel buttonPanel = new JPanel(); // to avoid button resizing
    south.add(label);
    south.add(clock);
    south.add(buttonPanel);

    final JButton run = new JButton("Run");
    buttonPanel.add(run);

    frame.pack();
    frame.setVisible(true);

    run.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        label.setText("Running...");
        race.run();
        run.setEnabled(false);
      }
    });

    race.addRaceListener(new RaceListener() {
      public void action(RaceEvent e) {
        double time = e.getTime() / 1000.0;
        int secs = (int) time;
        clock.setText(secs + "." + (int) ((time - secs) * 10));
        if (e.getWinner() != -1) {
          label.setText("Race won by #" + e.getWinner());
          run.setEnabled(true);
        }
      }
    });
  }
}
