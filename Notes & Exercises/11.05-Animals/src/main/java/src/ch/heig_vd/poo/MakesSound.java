package ch.heig_vd.poo;

public interface MakesSound {
  void makeSound();
}
