package ch.heig_vd.poo;

public class Person implements MakesSound {
  private String name;

  public Person(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  @Override
  public String toString() {
    return "Person «" + name + "»";
  }

  @Override
  public void makeSound() {
    System.out.println("Hi!");
  }
}
