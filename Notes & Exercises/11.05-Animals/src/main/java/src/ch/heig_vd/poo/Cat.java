package ch.heig_vd.poo;

public class Cat implements Animal {
  String name;
  Person owner;

  public Cat(String name, Person owner) {
    this.name = name;
    this.owner = owner;
  }

  @Override
  public Person getOwner() {
    return owner;
  }

  @Override
  public void makeSound() {
    System.out.println("Meow...");
  }

  @Override
  public String toString() {
    return "Le chat «" + name + "» de " + owner.getName();
  }
}
