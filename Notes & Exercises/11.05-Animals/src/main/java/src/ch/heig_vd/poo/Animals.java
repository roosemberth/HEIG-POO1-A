package ch.heig_vd.poo;

public class Animals {
  public static void main(String... args) {
    Person paul = new Person("Paul");
    Person ringo = new Person("Ringo");
    Person people[] = { paul, ringo };
    Animal animals[] = {
      new Dog("Rewolf", ringo),
      new Dog("Robert", ringo),
      new Cat("Shampoing", paul)
    };

    System.out.println("Tout le monde se présente !");
    for (Object o : animals)
      System.out.println(o);
    for (Object o : people)
      System.out.println(o);

    System.out.println("Les gens font du bruit !");
    for (Person p : people) {
      System.out.print(p + " says... ");
      p.makeSound();
    }

    System.out.println("Les animaux font du bruit !");
    for (Animal a : animals) {
      System.out.print(a + " says... ");
      a.makeSound();
    }

    System.out.println("Les chiens de Paul :");
    for (Animal a : animals) {
      if (a instanceof Dog && a.getOwner() == paul)
        System.out.println(a);
    }
  }
}
