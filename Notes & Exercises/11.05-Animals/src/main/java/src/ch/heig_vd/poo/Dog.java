package ch.heig_vd.poo;

public class Dog implements Animal {
  String name;
  Person owner;

  public Dog(String name, Person owner) {
    this.name = name;
    this.owner = owner;
  }

  @Override
  public Person getOwner() {
    return owner;
  }

  @Override
  public void makeSound() {
    System.out.println("Waf-waf!");
  }

  @Override
  public String toString() {
    return "Le chien «" + name + "» de " + owner.getName();
  }
}
