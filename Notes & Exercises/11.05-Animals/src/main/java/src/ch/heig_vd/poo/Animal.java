package ch.heig_vd.poo;

public interface Animal extends MakesSound {
  Person getOwner();
}
