# Exercise Animals

### Handout

> classes:
>   - Animal
>   - Person (nom; toString, noise)
>   - Dog (nom, propietaire; toString(), "Waf-waf")
>   - Liste personnes & chiens
>   - Afficher, Faire du bruit à tout le monde
>   - Faire aboyer les chiens de la personne Paul

### Running

```
$ mvn clean compile exec:java -Dexec.mainClass=ch.heig_vd.poo.Animals
```
