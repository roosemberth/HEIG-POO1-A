package main.java;

public class Subtraction extends ElementWiseOperation {
    /** Performs an element-wise subtraction
     *  @see ElementWiseOperation
     */
    public Subtraction(Matrix m1, Matrix m2) {
        super(m1, m2);
    }

    @Override
    int unboundedBinaryOp(int a1, int a2) {
        return a1 - a2;
    }
}
