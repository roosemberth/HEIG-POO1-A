package main.java;

import java.util.Scanner;

public class Lab5 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        // Store matrix attrs in arrays
        int[] mods = new int[2];
        int[] rows = new int[2];
        int[] columns = new int[2];

        // Read user-input
        for (int i = 0; i < 2; i++) {
            int n = i + 1;
            System.out.print("The modulus for matrix " + n + " is ");
            mods[i] = scan.nextInt();
            System.out.print("The row number for matrix " + n + " is ");
            rows[i] = scan.nextInt();
            System.out.print("The column number for matrix " + n + " is ");
            columns[i] = scan.nextInt();
            System.out.println();
        }
        scan.close();
        System.out.println();

        // 1st matrix
        System.out.println("one");
        Matrix m1 = new Matrix(mods[0], rows[0], columns[0]);
        m1.show();
        System.out.println();

        // 2nd matrix
        System.out.println("two");
        Matrix m2 = new Matrix(mods[1], rows[1], columns[1]);
        m2.show();
        System.out.println();

        // Addition operation
        System.out.println("one + two");
        Addition add = new Addition(m1, m2);
        add.result().show();
        System.out.println();

        // Subtraction operation
        System.out.println("one - two");
        Subtraction subtract = new Subtraction(m1, m2);
        subtract.result().show();
        System.out.println();

        // Multiplication operation
        System.out.println("one x two");
        Multiplication multiply = new Multiplication(m1, m2);
        multiply.result().show();
        System.out.println();
    }
}
