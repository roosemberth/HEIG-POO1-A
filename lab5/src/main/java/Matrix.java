package main.java;

import static java.lang.Math.*;

public class Matrix {
    private final int row, column, mod;
    private final int mat[][];

    /** Construct a matrix with the specified size full of
     *  random values.
     *  @param mod modulo of the elements
     *  @param row number of rows
     *  @param column number of columns
     *  @throws RuntimeException if mod is not positif
     */
    public Matrix(int mod, int row, int column) {
        this.mod = mod;
        this.row = row;
        this.column = column;
        this.mat = new int[row][column];

        if (mod <= 0) {
            throw new RuntimeException("Invalid modulo!");
        }

        for(int i = 0; i < this.row; i++) {
            for(int j = 0; j < this.column; j++) {
                mat[i][j] = floorDiv((int)(random() * (mod - 1)), mod);
            }
        }
    }

    /** Construct a matrix with the specified elements
     *  @param mod modulo
     *  @param mat list of knowng elements
     *  @throws RuntimeException if mod is not positif
     */
    public Matrix(int mod, int mat[][]) {
        if (mod <= 0) {
            throw new RuntimeException("Invalid modulo!");
        }
        this.mod = mod;
        this.row = mat.length;
        this.column = mat[0].length;
        this.mat = new int[row][column];

        for(int i = 0; i < this.row; i++) {
            for(int j = 0; j < this.column; j++) {
                this.mat[i][j] = floorDiv(mat[i][j], mod);
            }
        }
    }

    /** Prints the matrix to stdout */
    public void show() {
        for(int i = 0; i < this.row; i++) {
            for(int j = 0; j < this.column; j++) {
                System.out.print(this.mat[i][j] + " ");
            }
            System.out.println();
        }
    }

    /** GETTER mod
     *  @return mod of the matrix
     */
    public int getMod() {
        return this.mod;
    }

    /** GETTER row
     *  @return row of the matrix
     */
    public int getRow() {
        return this.row;
    }

    /** GETTER column
     *  @return mod of the matrix
     */
    public int getColumn() {
        return this.column;
    }

    /** GETTER list of elements
     *  @return list of the matrix
     */
    public int[][] getMat() {
        return this.mat;
    }
}
