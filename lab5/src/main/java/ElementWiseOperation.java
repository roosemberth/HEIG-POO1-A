package main.java;

import static java.lang.Math.*;

public class ElementWiseOperation {
    final Matrix m1, m2;
    final int rows, cols, mod;

    /** Constructs an operation between two matrices.
     *  The matrices __MUST__ have the same modulo.
     *  @param m1 1st matrix
     *  @param m2 2nd matrix
     *  @throws RuntimeException If args modulos differ
     */
    public ElementWiseOperation(Matrix m1, Matrix m2) {
        this.m1 = m1;
        this.m2 = m2;

        if(m1.getMod() != m2.getMod()) {
            throw new RuntimeException(
                "Cannot perform operations on matrices with different modulos"
            );
        }

        this.mod = m1.getMod();
        this.rows = max(m1.getRow(), m2.getRow());
        this.cols = max(m1.getColumn(), m2.getColumn());
    }

    /** Method to be overriden by element-wise operations.
     *  The result here is unbounded, and will be fitted
     *  mod n in the result function.
     *  @return the result of such operation
     */
    int unboundedBinaryOp(int a1, int a2) {
        // FIXME: This should be an abstract method.
        throw new RuntimeException("An operation must be defined by subclass");
    }

    /** Calculates the result of an operation on two matrices
     *  @return The matrix upon applying the binary operation
     */
    public Matrix result() throws RuntimeException {
        int[][] res = new int[rows][cols];
        int[] zeros = new int[cols];
        int m1Rows = m1.getRow();
        int m2Rows = m2.getRow();

        for (int i = 0; i < zeros.length; ++i) {
            zeros[i] = 0;
        }

        for(int i = 0; i < rows; i++) {
            int[] r1, r2;
            r1 = i < m1Rows ? m1.getMat()[i] : zeros;
            r2 = i < m2Rows ? m2.getMat()[i] : zeros;

            for(int j = 0; j < cols; j++) {
                int e1 = j < r1.length ? r1[j] : 0;
                int e2 = j < r2.length ? r2[j] : 0;
                int val = unboundedBinaryOp(e1, e2) + mod;
                res[i][j] = floorMod(val, mod);
            }
        }

        return new Matrix(mod, res);
    }
}
