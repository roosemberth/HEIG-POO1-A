package main.java;

public class Multiplication extends ElementWiseOperation {
    /** Performs an element-wise multiplication
     *  @see ElementWiseOperation
     */
    public Multiplication(Matrix m1, Matrix m2) {
        super(m1, m2);
    }

    @Override
    int unboundedBinaryOp(int a1, int a2) {
        return a1 * a2;
    }
}
