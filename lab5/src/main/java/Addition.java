package main.java;

import static java.lang.Math.*;

public class Addition extends ElementWiseOperation {
    /** Performs an addition on two matrices.
     *  @see ElementWiseOperation
     */
    public Addition(Matrix m1, Matrix m2) {
        super(m1, m2);
    }

    @Override
    int unboundedBinaryOp(int a1, int a2) {
        return a1 + a2;
    }
}
