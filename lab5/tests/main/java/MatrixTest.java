package main.java;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class MatrixTest {
    final Matrix m1 = new Matrix(5, new int[][]{
        new int[]{1, 3, 1, 1},
        new int[]{3, 2, 4, 2},
        new int[]{1, 0, 1, 0},
    });
    final Matrix m2 = new Matrix(5, new int[][]{
        new int[]{1, 4, 2, 3, 2},
        new int[]{0, 1, 0, 4, 2},
        new int[]{0, 0, 2, 0, 2},
    });

    void assertMatrixMod(final Matrix m, int mod) {
        for (final int[] row : m.getMat())
            for (final int e : row)
                assertTrue(e >= 0 && e < mod);
    }

    void assertEqualMatrices(final Matrix m1, final Matrix m2) {
        final int[][] rs1 = m1.getMat();
        final int[][] rs2 = m2.getMat();
        assertEquals(rs1.length, rs2.length);
        for (int i = 0; i < rs1.length; ++i) {
            final int[] cs1 = rs1[i];
            final int[] cs2 = rs2[i];
            assertEquals(cs1.length, cs2.length);
            for (int j = 0; j < cs1.length; ++j) {
                assertEquals(cs1[j], cs2[j]);
            }
        }
    }

    @Test
    void testCreateMatrix() {
        final Matrix m = new Matrix(10, 4, 5);
        assertMatrixMod(m, 10);
    }

    @Test
    void testMatrixAdd() {
        Matrix expected = new Matrix(5, new int[][]{
            new int[]{2, 2, 3, 4, 2},
            new int[]{3, 3, 4, 1, 2},
            new int[]{1, 0, 3, 0, 2},
        });
        assertEqualMatrices(expected, new Addition(m1, m2).result());
        assertEqualMatrices(expected, new Addition(m2, m1).result());

        // m1 = m1 + 5*m1 (mod 5)
        Matrix t5 = m1;
        for (int i = 0; i < 5; ++i)
            t5 = new Addition(t5, m1).result();
        assertEqualMatrices(m1, t5);
    }

    @Test
    void testMatrixMinus() {
        Matrix expected = new Matrix(5, new int[][]{
            new int[]{0, 4, 4, 3, 3},
            new int[]{3, 1, 4, 3, 3},
            new int[]{1, 0, 4, 0, 3},
        });
        assertEqualMatrices(expected, new Subtraction(m1, m2).result());

        // m1 = m1 - 5*m1 (mod 5)
        Matrix t5 = m1;
        for (int i = 0; i < 5; ++i)
            t5 = new Subtraction(t5, m1).result();
        assertEqualMatrices(m1, t5);
    }

    @Test
    void testMatrixElementProduct() {
        Matrix expected = new Matrix(5, new int[][]{
            new int[]{1, 2, 2, 3, 0},
            new int[]{0, 2, 0, 3, 0},
            new int[]{0, 0, 2, 0, 0},
        });
        assertEqualMatrices(expected, new Multiplication(m1, m2).result());

        // m1 = m1**(p-1) (mod p) -- Fermat's little theorem
        Matrix tm = m1;
        for (int i = 0; i < tm.getMod() - 1; ++i)
            tm = new Multiplication(m1, tm).result();
        assertEqualMatrices(m1, tm);
    }

    @Test
    void testMatrixSumMinus() {
        Matrix r1 = new Addition(m1, m2).result();
        Matrix r2 = new Subtraction(m1, m2).result();

        // A m1-sized zero-matrix.
        Matrix m1_z = new Subtraction(m1, m1).result(); // m1 - m1 = {0}
        // A m2-sized zero-matrix.
        Matrix m2_z = new Subtraction(m2, m2).result(); // m2 - m2 = {0}

        // Mix with the other-matrix size to fix sizes.
        Matrix m1_2 = new Addition(m2_z, new Addition(m1, m1).result()).result();
        Matrix m2_2 = new Addition(m1_z, new Addition(m2, m2).result()).result();

        // (m1 + m2) + (m1 - m2) = 2 * m1
        assertEqualMatrices(m1_2, new Addition(r1, r2).result());

        // (m1 + m2) - (m1 - m2) = 2 * m2
        assertEqualMatrices(m2_2, new Subtraction(r1, r2).result());
    }
}
