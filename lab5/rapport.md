<!-- This document may be rendered using the following commands:
pandoc rapport.md -f markdown -F pandoc-plantuml -o lab5-report.pdf
-->
---
title: POO-A 2020 Lab 5
author:
  - Roosembert Palacios
  - Mai Hoang Anh
geometry:
- top=1in
- left=1in
- right=1in
- bottom=0.7in
header-includes:
- \usepackage{setspace}
- \usepackage{lscape}
- \usepackage{multicol}
---
<!-- (C) Roosembert Palacios, 2020
         Mai Hoang Anh, 2020

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
-->

\newcommand{\blandscape}{\begin{landscape}}
\newcommand{\elandscape}{\end{landscape}}
\newcommand{\bcolumns}{\begin{multicols}{3}}
\newcommand{\ecolumns}{\end{multicols}}

## Organisation

### Déroulement du laboratoire

Une implémentation initiale a été faite pour les étapes 1 et 2. Pour l'étape
numéro 3, un gabarit d'implémentation a été commencé mais interrompu en cours de
route.

Nous avons décidé de prendre du recul et procéder à la conception d'un schéma de
classes adéquat au problème.
Une fois celui-ci terminé, nous avons écrit certains tests élémentaires (basés
sur l'exemple d'exécution dans l'énoncé).

Par la suite le code a été _largement_ modifié afin d'implémenter ce que le
schéma décrit. Finalement nous avons décidé d'écrire encore quelques tests afin
de vérifier des propriétés du code (plutôt que des valeurs concrètes).

## Schéma de classes pour l'étape 3

La classe `Lab5` qui se charge de l'exécution de test n'a pas été modélisé.
Les classes de test n'ont pas été modélisés non plus.

```{.plantuml caption="Matrix and operations" format="svg" width=400px}
@startuml
skinparam monochrome true
hide methods

class Matrix {
  row : int
  column : int
  mod : int
  mat : int[][]
}

class ElementWiseOperation {
  rows : int
  cols : int
  mod : int
}

Matrix "1" -r- "*" ElementWiseOperation : First operand
Matrix "1" -r- "*" ElementWiseOperation : Second operand

class Addition
hide Addition attributes
ElementWiseOperation <|-- Addition

class Multiplication
hide Multiplication attributes
ElementWiseOperation <|-- Multiplication

class Subtraction
hide Subtraction attributes
ElementWiseOperation <|-- Subtraction
@enduml
```

## Tests effectués

Des tests on été écrits seulement à partir de l'étape 3.

Nous avons commencé par écire des tests concrets sur la base de la sortie
d'exemple donné dans l'énoncé.
Par la suite, nous avons procédé à ajouter des tests plus mathématiques, tels
que des propriétés des groupes cycliques et l'exponentiation en modulo (i.e.
Petit théorème de Fermat).

Rien de particulier à signaler sur la conception, implémentation ou exécution de
ces tests.

\newgeometry{left=6mm, bottom=1cm, right=6mm, top=3cm}
\blandscape

\Large
**Annexe A: Code** \scriptsize
(Le code ci-dessous a été adapté au format A4. Notamment
des élément de syntaxe ont été modifiés et certaines
implémentations triviales ou tests ont étés omis.)

\bcolumns
\scriptsize

```java
public class Matrix {
    private final int row, column, mod;
    private final int mat[][];

    /** Construct a matrix with the specified size full of
     *  random values.
     *  @param mod modulo of the elements
     *  @param row number of rows
     *  @param column number of columns
     *  @throws RuntimeException if mod is not positive
     */
    public Matrix(int mod, int row, int column) {
        this.mod = mod;
        this.row = row;
        this.column = column;
        this.mat = new int[row][column];
        if (mod <= 0)
            throw new RuntimeException("Invalid modulo!");
        for(int i = 0; i < this.row; i++)
            for(int j = 0; j < this.column; j++)
                mat[i][j] = floorDiv((int)(random() * (mod - 1)), mod);
    }

    /** Construct a matrix with the specified elements
     *  @param mod modulo of the elements
     *  @param mat list of knowng elements
     *  @throws RuntimeException if mod is not positif
     */
    public Matrix(int mod, int mat[][]) {
        this.mod = mod;
        this.row = mat.length;
        this.column = mat[0].length;
        this.mat = new int[row][column];
        if (mod <= 0)
            throw new RuntimeException("Invalid modulo!");
        for(int i = 0; i < this.row; i++)
            for(int j = 0; j < this.column; j++)
                this.mat[i][j] = floorDiv(mat[i][j], mod);
    }

    /** Prints the matrix to stdout */
    public void show() {
        for(int i = 0; i < this.row; i++) {
            for(int j = 0; j < this.column; j++)
                System.out.print(this.mat[i][j] + " ");
            System.out.println();
        }
    }
    // Getters for mod, row, colums and mat omitted
    // brevety...
}

public class ElementWiseOperation {
    Matrix m1, m2;
    int rows, cols, mod;

    /** Constructs an operation between two matrices.
     *  The matrices __MUST__ have the same modulo.
     *  @param m1 1st matrix
     *  @param m2 2nd matrix
     *  @throws RuntimeException If args modulos differ
     */
    public ElementWiseOperation(Matrix m1, Matrix m2) {
        this.m1 = m1;
        this.m2 = m2;

        if(m1.getMod() != m2.getMod())
            throw new RuntimeException(
              "Cannot perform operations on " +
              "matrices with different modulos");

        this.mod = m1.getMod();
        this.rows = max(m1.getRow(), m2.getRow());
        this.cols = max(m1.getColumn(), m2.getColumn());
    }

    /** Method to be overriden by element-wise operations.
     *  The result here is unbounded, and will be fitted
     *  mod n in the result function.
     *  @return the result of such operation
     */
    int unboundedBinaryOp(int a1, int a2) {
        // FIXME: This should be an abstract method.
        throw new RuntimeException("An operation must " +
          "be defined by subclass");
    }

    /** Calculates the result of an operation on two matrices
     *  @return The matrix upon applying the binary operation
     */
    public Matrix result() {
        int[][] res = new int[rows][cols];
        int[] zeros = new int[cols];
        int m1Rows = m1.getRow(), m2Rows = m2.getRow();
        for (int i = 0; i < zeros.length; ++i)
            zeros[i] = 0;

        for(int i = 0; i < rows; i++) {
            int[] r1, r2;
            r1 = i < m1Rows ? m1.getMat()[i] : zeros;
            r2 = i < m2Rows ? m2.getMat()[i] : zeros;

            for(int j = 0; j < cols; j++) {
                int e1 = j < r1.length ? r1[j] : 0;
                int e2 = j < r2.length ? r2[j] : 0;
                int val = unboundedBinaryOp(e1, e2) + mod;
                res[i][j] = floorMod(val, mod);
            }
        }
        return new Matrix(mod, res);
    }
}

public class Addition extends ElementWiseOperation {
    /** Performs an addition on two matrices.
     *  @see ElementWiseOperation
     */
    public Addition(Matrix m1, Matrix m2) {
        super(m1, m2);
    }
    @Override
    int unboundedBinaryOp(int a1, int a2) {
        return a1 + a2;
    }
}

public class Multiplication extends ElementWiseOperation {
    /** Performs an element-wise multiplication
     *  @see ElementWiseOperation
     */
    public Multiplication(Matrix m1, Matrix m2) {
        super(m1, m2);
    }
    @Override
    int unboundedBinaryOp(int a1, int a2) {
        return a1 * a2;
    }
}

public class Subtraction extends ElementWiseOperation {
    /** Performs an element-wise subtraction
     *  @see ElementWiseOperation
     */
    public Subtraction(Matrix m1, Matrix m2) {
        super(m1, m2);
    }
    @Override
    int unboundedBinaryOp(int a1, int a2) {
        return a1 - a2;
    }
}

public class Lab5 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        // Store matrix attrs in arrays
        int[] mods = new int[2];
        int[] rows = new int[2];
        int[] columns = new int[2];

        // Read user-input
        for (int i = 0; i < 2; i++) {
            int n = i + 1;
            System.out.print("The modulus for matrix " + n + " is ");
            mods[i] = scan.nextInt();
            System.out.print("The row number for matrix " + n + " is ");
            rows[i] = scan.nextInt();
            System.out.print("The column number for matrix " + n + " is ");
            columns[i] = scan.nextInt();
            System.out.println();
        }
        scan.close();
        System.out.println();

        // 1st matrix
        System.out.println("one");
        Matrix m1 = new Matrix(mods[0], rows[0], columns[0]);
        m1.show();
        System.out.println();

        // 2nd matrix
        System.out.println("two");
        Matrix m2 = new Matrix(mods[1], rows[1], columns[1]);
        m2.show();
        System.out.println();

        // Addition operation
        System.out.println("one + two");
        Addition add = new Addition(m1, m2);
        add.result().show();
        System.out.println();

        // Subtraction operation
        System.out.println("one - two");
        Subtraction subtract = new Subtraction(m1, m2);
        subtract.result().show();
        System.out.println();

        // Multiplication operation
        System.out.println("one x two");
        Multiplication multiply = new Multiplication(m1, m2);
        multiply.result().show();
        System.out.println();
    }
}
```
\ecolumns
\elandscape
\restoregeometry
\normalsize
