# Lab5 POO1

## Running the application

```
$ mvn clean compile exec:java -Dexec.mainClass=main.java.Lab5
```

## Running the tests

```
$ mvn clean compile test
```

## Rendering report

```
$ pandoc rapport.md -f markdown -F pandoc-plantuml -o lab5-report.pdf
```
