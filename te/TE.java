import java.util.LinkedList;

// Nom: PALACIOS Roosemberth

class Element {
  public static final Element fer = new Element("fer", 1);
  public static final Element carbone = new Element("carbone", 2);
  public static final Element mercure = new Element("mercure", 3);

  private final String name;
  private final int id;

  private Element(String name, int id) {
    this.name = name;
    this.id = id;
  }

  @Override
  public String toString() {
    return this.name + " (#" + this.id + ")";
  }

  public static Element[] values() {
    return new Element[] { fer, carbone, mercure };
  }
}

abstract class Extracteur {
  private int id;
  private String prefix_nom;

  Extracteur(String prefix_nom, int id) {
    this.id = id;
    this.prefix_nom = prefix_nom;
  }

  public abstract Object extraire();

  public String nom() {
    return this.prefix_nom + "-" + id;
  };
}

class Planete {
  private String nom;
  private Element[] elements;
  private int nSondes = 0;

  public Planete(String nom, Element... elements) {
    this.nom = nom;
    this.elements = elements.clone();
  }

  public String nom() {
    return nom;
  }

  public boolean contient(Element element) {
    for (Element e : elements)
      if (e == element)
        return true;
    return false;
  }

  public Extracteur construireSonde(Element e) {
    Extracteur sonde = new Extracteur(nom, 1 + nSondes++) {
      public Object extraire() {
        System.out.print(nom() + " recherche " + e + "... ");

        if (contient(e)) {
          System.out.println("extrait !");
          return e;
        }

        System.out.println("inconnu");
        return null;
      };
    };
    System.out.println("Construction de la sonde " + sonde.nom());
    return sonde;
  }
}

class TE {
  public static void main(String[] args) {
    System.out.println("-- Elements connus");
    for (Element e : Element.values())
      System.out.println(e);

    Planete mars = new Planete("Mars", Element.fer, Element.carbone);
    Planete venus = new Planete("Venus", Element.mercure);

    System.out.println("-- Construction des sondes");

    Extracteur[] sondes = {
      mars.construireSonde(Element.mercure),
      mars.construireSonde(Element.carbone),
      venus.construireSonde(Element.fer),
      venus.construireSonde(Element.mercure),
    };

    System.out.println("-- Extractions");

    LinkedList<Object> list = new LinkedList<>();
    for (Extracteur s : sondes) {
      Object e = s.extraire();
      if (e != null)
        list.add(e);
    }

    System.out.println("-- Eléments trouvés");
    System.out.println(list);
  }
}
