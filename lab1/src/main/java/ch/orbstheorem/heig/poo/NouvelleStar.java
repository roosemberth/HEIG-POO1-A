package ch.orbstheorem.heig.poo;

import java.util.Random;

public class NouvelleStar {
  public static void main(String args[]) {
    if (args.length == 0) {
      System.out.println("Il n'existe pas de candidats !");
      System.exit(1);
    }

    System.out.println("Candidats:");
    for (int i = 0; i < args.length; ++i)
      System.out.println("#" + (i + 1) + " " + args[i]);

    int candidates = args.length;
    int[] votes = new int[candidates];
    Random random = new Random();
    System.out.println();
    System.out.println("150 votes:");

    for (int i = 0; i < 150; ++i) {
      votes[random.nextInt(candidates)]++;
    }

    System.out.println("Résultats:");
    for (int i = 0; i < args.length; ++i)
      System.out.println(Math.round(votes[i] * 100 / 150) + "% " + args[i]);
  }
}
