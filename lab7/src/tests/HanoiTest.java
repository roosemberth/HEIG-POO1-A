import hanoi.*;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

public class HanoiTest {

    @Test
    void testCreateHanoi() {
        assertThrows(RuntimeException.class, ()->{new Hanoi(0, new HanoiDisplayer());});
        final Hanoi h = new Hanoi(2);
        assertEquals(2, h.getPoles()[0].tabObjects()[1]);
        assertEquals(1, h.getPoles()[0].tabObjects()[0]);
    }

    @Test
    void testHanoiMove() {
        final Hanoi h = new Hanoi(3);
        h.solve();
        assertEquals(3, h.getPoles()[2].tabObjects()[2]);
        assertEquals(2, h.getPoles()[2].tabObjects()[1]);
        assertEquals(1, h.getPoles()[2].tabObjects()[0]);
    }
}
