import org.junit.jupiter.api.Test;
import util.Stack;

import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;

public class StackTest {

    @Test
    void testCreateStack() {
        final Stack s = new Stack(4);
        assertTrue(s.isEmpty());
        assertThrows(RuntimeException.class, ()->{new Stack(-1);});
    }

    @Test
    void testStackPop() {
        final Stack s = new Stack(4);
        assertThrows(RuntimeException.class, s::pop);

        s.push(3);
        s.push(5);
        assertEquals(5, s.pop());
        assertEquals(3, s.pop());
    }

    @Test
    void testStackPush() {
        final Stack s = new Stack(0);
        assertThrows(StackOverflowError.class, ()->{s.push(5);});
    }

    @Test
    void testStackIterator() {
        final Stack s = new Stack(2);
        s.push(1);
        s.push(2);
        Iterator<Object> it = s.iterator();
        assertTrue(it.hasNext());
        assertEquals(2, it.next());
        assertTrue(it.hasNext());
        assertEquals(1, it.next());
        assertFalse(it.hasNext());
    }
}
