package hanoi;

import util.Stack;

public class Hanoi {

    private final int nbDisks;
    private Stack poles[] = new Stack[3];
    private HanoiDisplayer hd;

    private int turn = 0;

    /** Hanoi Tower generic constructor with the specified size and
     *  specified display.
     *  @param nbDisks modulo of the elements
     *  @param displayer how to display the tower
     *  @throws RuntimeException if there is disk to move
     */
    public Hanoi(int nbDisks, HanoiDisplayer displayer) throws RuntimeException {
        if (nbDisks <= 0) {
            throw new RuntimeException("No disk to move !");
        }

        this.nbDisks = nbDisks;

        poles[0] = new Stack(nbDisks);
        poles[1] = new Stack(nbDisks);
        poles[2] = new Stack(nbDisks);

        for (int i = nbDisks; i >= 1; i--) {
            poles[0].push(i);
        }
        hd = displayer;
    }

    /** Hanoi Tower generic constructor with the specified size and
     *  console display by default
     *  @param nbDisks modulo of the elements
     */
    public Hanoi(int nbDisks) {
        this(nbDisks, new HanoiDisplayer());
    }

    /** GETTER poles
     *  @return 3 poles of Hanoi Tower
     */
    public Stack[] getPoles() {
        return poles;
    }

    /** Auxiliary function that moves a disk from a pole to
     *  another pole
     *  @param poleSrc source pole
     *  @param poleDst destination pole
     */
    public void moveDisks(Stack poleSrc, Stack poleDst) {
        if (poleSrc.isEmpty() && poleDst.isEmpty()) {
            throw new RuntimeException("Both poles are empty!");
        } else {
            if (poleSrc.isEmpty()) {
                Integer topDst = (Integer) poleDst.pop();
                poleSrc.push(topDst);
            } else if (poleDst.isEmpty()) {
                Integer topSrc = (Integer) poleSrc.pop();
                poleDst.push(topSrc);
            } else {
                Integer topSrc = (Integer) poleSrc.pop();
                Integer topDst = (Integer) poleDst.pop();

                if (topSrc > topDst) {
                    poleSrc.push(topSrc);
                    poleSrc.push(topDst);
                } else {
                    poleDst.push(topDst);
                    poleDst.push(topSrc);
                }
            }
        }
    }

    /** Move all the disks from the first pole
     *  to the third pole using the auxiliary function
     */
    public void solve() {
        hd.display(this);

        while (!finished()) {
            turn++;

            if (turn % 3 == 1) {
                if (nbDisks % 2 == 0) {
                    moveDisks(poles[0], poles[1]);
                } else {
                    moveDisks(poles[0], poles[2]);
                }
            } else if (turn % 3 == 2) {
                if (nbDisks % 2 == 0) {
                    moveDisks(poles[0], poles[2]);
                } else {
                    moveDisks(poles[0], poles[1]);
                }
            } else if (turn % 3 == 0) {
                moveDisks(poles[1], poles[2]);
            }

            hd.display(this);
        }
    }

    /** Get state of 3 poles
     *  @return 2D array where rows represent poles
     *  and columns represent number of disks
     */
    public int[][] status() {
        int[][] res = new int[3][];
        for (int i = 0; i < 3; i++) {
            int size = poles[i].tabObjects().length;
            res[i] = new int[size];

            for (int j = 0; j < size; j++) {
                res[i][j] = (Integer) poles[i].tabObjects()[j];
            }
        }
        return res;
    }

    /**
     *  @return true if the algorithm is finished
     *  false, otherwise
     */
    public boolean finished() {
        return turn == Math.pow(2, nbDisks) - 1;
    }

    /** GETTER turn
     *  @return turn
     */
    public int turn() {
        return turn;
    }
}
