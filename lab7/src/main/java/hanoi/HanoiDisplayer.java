package hanoi;

public class HanoiDisplayer {

    /** Hanoi Tower console display
     *  @param h Hanoi Tower to show
     */
    public void display(Hanoi h) {

        System.out.println("-- Turn: " + h.turn());

        System.out.print("One:   ");
        System.out.print(h.getPoles()[0].toString());
        System.out.println();

        System.out.print("Two:   ");
        System.out.print(h.getPoles()[1].toString());
        System.out.println();

        System.out.print("Three: ");
        System.out.print(h.getPoles()[2].toString());
        System.out.println();

        System.out.println();
    }
}
