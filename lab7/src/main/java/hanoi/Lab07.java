package hanoi;

import hanoi.gui.JHanoi;

public class Lab07 {


    private static void usage() {
        System.out.println(
                "This program may be invoked with an int or the string"
                        + " \"gui\". If an int is provided, a terminal interface will be"
                        + " used, otherwise a graphical window will appear.");
    }

    public static void main(String... args) {
        if (args.length != 1) {
            usage();
            return;
        }


        if ("gui".equals(args[0])) {
            new JHanoi();
        } else {
            int size;
            try {
                size = Integer.parseInt(args[0]);
            } catch (Exception e) {
                usage();
                return;
            }
            new Hanoi(size, new HanoiDisplayer()).solve();
        }
    }
}
