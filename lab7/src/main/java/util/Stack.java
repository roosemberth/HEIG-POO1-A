package util;

import java.util.ArrayList;
import java.util.Iterator;

public class Stack implements Iterable {

    // list storing elements
    private ArrayList<Object> list;

    // capacity of stack
    private final int size;

    /** Stack constructor with the specified size
     *  @param size stack capacity
     *  @throws RuntimeException if size is negative
     */
    public Stack(int size) throws RuntimeException {
        if(size < 0) {
            throw new RuntimeException("Invalid stack size!");
        }
        list = new ArrayList<>();
        this.size = size;
    }

    /** Check if stack is full
     *  @return true if top index is smaller than size by 1
     *  false, otherwise
     */
    public boolean isFull() {
        return list.size() == size;
    }

    /** Check if stack is empty
     *  @return true if top index is equal to -1
     *  false, otherwise
     */
    public boolean isEmpty() {
        return list.isEmpty();
    }

    /** Pop top element from the stack
     *  @throws RuntimeException if stack is empty
     *  @return top element of the stack
     */
    public Object pop() throws RuntimeException {
        if (isEmpty()) {
            throw new RuntimeException("Stack Underflow!");
        }
        Object o = list.get(0);
        list.remove(0);
        return o;
    }

    /** Push an element to top of the stack
     *  @throws StackOverflowError if stack is full
     */
    public void push(Object o) throws StackOverflowError {
        if (isFull()) {
            throw new StackOverflowError("Stack Overflow!");
        }
        list.add(0, o);
    }

    /**
     *  Representation in string
     */
    public String toString() {
        StringBuilder str = new StringBuilder("[ ");
        if (!isEmpty()) {
            for (int i = 0; i < tabObjects().length; i++) {
                str.append("<").append(list.get(i)).append("> ");
            }
        }
        str.append("]");
        return str.toString();
    }

    /**
     *  @return array of objects showing state of the stack
     */
    public Object[] tabObjects() {
        return list.toArray();
    }

    /**
     *  @return iterator of the stack
     */
    @Override
    public Iterator<Object> iterator() {
        return new Iterator<Object>() {
            private int currentIndex = 0;

            @Override
            public boolean hasNext() {
                return currentIndex < list.size();
            }

            @Override
            public Object next() {
                return list.get(currentIndex++);
            }

            @Override
            public void remove() {
                throw new RuntimeException("Not implemented");
            }
        };
    }

}
